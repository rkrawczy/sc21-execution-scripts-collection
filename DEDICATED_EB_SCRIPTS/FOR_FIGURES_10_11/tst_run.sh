#!/bin/bash

params1="
-x UCX_NET_DEVICES=mlx5_0:1
-x UCX_IB_GID_INDEX=5
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib
--report-bindings
"

params2="
-x UCX_NET_DEVICES=mlx5_1:1 
-x UCX_IB_GID_INDEX=5
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib
--report-bindings
"

params3="
-x UCX_NET_DEVICES=mlx5_3:1 
-x UCX_IB_GID_INDEX=5
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib
--report-bindings
"


#for the test with pfc and ecn
params_pfc_ecn="
-x UCX_RC_VERBS_TRAFFIC_CLASS=24
-x UCX_RC_MLX5_TRAFFIC_CLASS=24
-x UCX_DC_VERBS_TRAFFIC_CLASS=24
-x UCX_DC_MLX5_TRAFFIC_CLASS=24
-x UCX_UD_VERBS_TRAFFIC_CLASS=24
-x UCX_UD_MLX5_TRAFFIC_CLASS=24
-x UCX_RC_VERBS_SL=3
-x UCX_RC_MLX5_SL=3
-x UCX_DC_MLX5_SL=3
-x UCX_UD_VERBS_SL=3
-x UCX_UD_MLX5_SL=3
-x UCX_CM_SL=3
"

#for the test with pfc
params_pfc="
-x UCX_RC_VERBS_SL=3
-x UCX_RC_MLX5_SL=3
-x UCX_DC_MLX5_SL=3
-x UCX_UD_VERBS_SL=3
-x UCX_UD_MLX5_SL=3
-x UCX_CM_SL=3
"

tt=7200
	
runargs="-t $tt"


wrapper_intel=" hwloc-bind --membind node:0 --cpubind node:0  /home/rkrawczy/CERN_19/DEDICATED_EB/dedicated-eb-stresstest/bin/benchmark $runargs" 
wrapper_amd=" hwloc-bind --membind node:1 --cpubind node:1   /home/rkrawczy/CERN_19/DEDICATED_EB/dedicated-eb-stresstest/bin/benchmark $runargs" 

mpirun -n 1 -npernode 1 --hostfile ./HOSTFILES/hosts_amd_tst $params2 $params_pfc  $wrapper_amd -F 72 : -n 14 -npernode 1 --hostfile ./HOSTFILES/full/hosts_amd_full $params2 $params_pfc  $wrapper_amd -F 72 : -n 36 -npernode 1 --hostfile ./HOSTFILES/full/hosts_hltv03_full $params2 $params_pfc $wrapper_intel -F 72 : -n 36 -npernode 1 --hostfile ./HOSTFILES/full/hosts_hltv03_full $params1 $params_pfc $wrapper_intel -F 72

