This is a collection of execution scripts of benchmarks used for the SC21 paper.
The scripts are for the sake of reproducibility of the tests.
The benchmarks used custom host files for the temporary setups.
To reproduce, one must use its host files. 

Overview of catalogs and files:

-DAQPIPE_RUN_SCRIPTS
Scripts used to run the DAQPIPE benchmark: 
https://gitlab.cern.ch/lhcb-online-eb/lhcb-daqpipe-v2

Contains scripts that were used to obtain figures 7 and 8 in the SC21 paper:
DAQPIPE_RUN_SCRIPTS/FOR_FIGURE_7
DAQPIPE_RUN_SCRIPTS/FOR_FIGURE_8

Test for figure 7 with a narrow parameter scan. 
Its purpose was to increase the number of nodes participating
 in the transmissions to saturate the Arista DCS-7280CR2K-30-F switch.


Test for figure 8 was made with a broad parameter scan to understand the network caveats 
and the switches limitations of Juniper 10000-30C and Arista DCS-7280CR2K-30-F.

Scripts were run with 100 Gb/s NICs.

-DEDICATED_EB_SCRIPTS
Scripts used to run the  dedicated_eb_stresstest benchmark:
https://gitlab.cern.ch/lhcb-online-eb/dedicated-eb-stresstest


Contains scripts that were used to obtain figures 10 and 11 in the SC21 paper:
DEDICATED_EB_SCRIPTS/FOR_FIGURES_10_11

Script was run with a Juniper QFX5200 switch, with 14 x 100 Gb/s NICs and 72 x 25 Gb/s links.

It was typically used to test a network performance with one shallow-buffered switch.

-DISTR_EB_SCRIPTS
Scripts used to run the Event builder benchmark 
https://gitlab.cern.ch/lhcb-online-eb/distributed_network_benchmark.

Contains scripts that were used to obtain figures 5 and 6 in the SC21 paper:
DISTR_EB_SCRIPTS/FOR_FIGURES_5_6


Arista 7280SR2A and Juniper QFX5200 were used as ToRs, Juniper 10000-30C were the core switches.
Its purpose was to evaluate the caveats of the tested, Ethernet-only network architecture. 
It was typically used for networks with at least one ToR and one core switch.


8 x 25 Gb/s NICs were connected to ToR, 100 Gb/s NICs were connected to the core switches. 

In all of the tests:

hwloc was used to bind to the correct NUMA domain on the nodes

gcc-6.4.0 was used as a compiler
