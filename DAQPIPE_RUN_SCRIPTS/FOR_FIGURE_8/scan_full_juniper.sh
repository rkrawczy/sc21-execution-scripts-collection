#!/bin/bash

MODE=$1
mode=$2

DIR=out4-$MODE-$mode-$np$4
mkdir -p $DIR
cp $0 $DIR/


if [ -z "$3" ]; then
        echo "USAGE : $0 {barrel|random} {PUSH|PULL} {16} [extra-dir-name]"
        exit 1
fi

params="
-x UCX_NET_DEVICES=mlx5_1:1 
-x UCX_IB_GID_INDEX=5
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib"


wrapper_intel="hwloc-bind --membind node:0 --cpubind node:0 /home/rkrawczy/CERN_18/lhcb-daqpipe-v2/build-mpi/src/daqpipe "

wrapper_amd="hwloc-bind --membind node:1 --cpubind node:1 /home/rkrawczy/CERN_18/lhcb-daqpipe-v2/build-mpi/src/daqpipe "


function genconf()
{
cat > $DIR/config-scan.json << EOF
{

        "drivers" : 
        {
                "slots" : 
                {
                        "cmdRecver" : 4,
                        "listenerSleep" : 100,
                        "maxOnFly" : 256,
                        "maxRdmaRecv" : 128,
                        "maxRdmaSend" : $rdma
                }
        },
        "gather" : 
        {
                "sched" : 
                {
                        "parallel" : $parallel,
                        "policy" : "$MODE"
                }
        },
        "general" : 
        {
			"eventRailMaxDataSize" : $size,
			"mode" : "${mode}",
			"dataSizeRandomRatio" : 0
        },
        "testing" : 
        {
			"dataChecking" : false,
			"skipMemcpy": true,
			"customTopology" : false,
			"useDriverThreads" : true,
			"useLocalDriverThreads" : true,
			"oneWay": true
        }
}

EOF
}

function getMean()
{
        echo $(cat $fname | grep MON | cut -d ' ' -f 10 | xargs echo | sed -e 's/ /+/g' | bc -l)/$(grep -c MON $fname) | bc -l | xargs printf "%.2f\n" | sed -e "s/,/./g"
}

function getMax()
{
        cat $fname | grep MON | cut -d ' ' -f 10 | sort -n | tail -n 1
}

echo "#credit	parallel	size	rdma	ppn	Mean	Max" >> $DIR/stats.log

for ppn in 2
do
	if [ $ppn -eq 1 ]; then
			oneway=false
	else
			oneway=true
	fi

	for rdma in 32 64 128 256 # 4 //4
	do
		for parallel in 1 2 4 8 16 32 #6 //24
		do
			for size in  131072 262144 524288 1048576 2097152 4194304 #6 //144
			do
				for credit in 1 2 4 8 #4 //576
				do
					fname=$DIR/out-${credit}-${parallel}-${size}-${rdma}-${ppn}.log
					echo --------------------------------------------------------------------------
					echo
					echo "credit=${credit} parallel=${parallel} size=${size} rdma=${rdma} ppn=${ppn}"
					echo
					if [ -f $fname ]; then continue; fi
					genconf
					#hydra-mpiexec -np 8 --ppn 1 --hostfile ~/hosts2 
					set -x
					#mpirun -np 12 -npernode $ppn --hostfile ./hostfiles/hosts_intel $params $wrapper_intel  -f $DIR/config-scan.json -c $credit -t 15 | tee $fname

					mpirun -np 48  -npernode $ppn --hostfile ./hostfiles/hosts_amd $params $wrapper_amd  -f $DIR/config-scan.json -c $credit -t 15 : -np 12 -npernode $ppn --hostfile ./hostfiles/hosts_intel $params $wrapper_intel  -f $DIR/config-scan.json -c $credit -t 15 | tee $fname	
	
					set +x
					cp $DIR/config-scan.json $fname.json
					echo "$credit	$parallel	$size	$rdma	$ppn	$(getMean)	$(getMax)" >> $DIR/stats.log
				done
			done
		done
	done
done

