#!/bin/bash

MODE=$1
mode=$2
np=$3

buru=$4

DIR=out4-$MODE-$mode-$buru-$np$5
mkdir -p $DIR
cp $0 $DIR/


if [ -z "$4" ]; then
        echo "USAGE : $0 {barrel|random} {PUSH|PULL} {16} {BU/RU number} [extra-dir-name] "
        exit 1
fi

params="
-x UCX_NET_DEVICES=mlx5_3:1 
-x UCX_IB_GID_INDEX=5
-x UCX_TLS=rc_x,sm,self
-bind-to none
--mca coll ^hcoll
--mca pml ucx 
--mca btl ^openib"


wrapper_intel="hwloc-bind --membind node:0 --cpubind node:0 /home/rkrawczy/CERN_18/lhcb-daqpipe-v2/build-mpi/src/daqpipe "

wrapper_amd="hwloc-bind --membind node:2 --cpubind node:2 /home/rkrawczy/CERN_18/lhcb-daqpipe-v2/build-mpi/src/daqpipe "


function genconf()
{
	
> $DIR/config-scan.json

cat >> $DIR/config-scan.json << EOF
{

        "drivers" : 
        {
                "slots" : 
                {
                        "cmdRecver" : 4,
                        "listenerSleep" : 100,
                        "maxOnFly" : 256,
                        "maxRdmaRecv" : 128,
                        "maxRdmaSend" : $rdma
                }
        },
        "gather" : 
        {
                "sched" : 
                {
                        "parallel" : $parallel,
                        "policy" : "$MODE"
                }
        },
        "general" : 
        {
			"eventRailMaxDataSize" : $size,
			"mode" : "${mode}",
			"dataSizeRandomRatio" : 0
        },
        "testing" : 
        {
			"dataChecking" : false,
			"skipMemcpy": true,
			"customTopology" : true,
			"useDriverThreads" : true,
			"useLocalDriverThreads" : true,
			"oneWay": true
        },
	"topology": [
       	[ "EM" ],
EOF

#for i in {1..$1}
for (( c=1; c<=$1; c++ ))
do
	cat >> $DIR/config-scan.json << EOF
		[ "BU" ],
EOF
done

#for i in {1..$($1-1)}

aux=$1-1
for (( c=1; c<=$aux; c++ ))
do
	cat >> $DIR/config-scan.json << EOF
		[ "RU" ],
EOF
done

cat >> $DIR/config-scan.json << EOF
		[ "RU" ]
EOF


cat >> $DIR/config-scan.json << EOF
     	]
}
EOF

}



function getMean()
{
        echo $(cat $fname | grep MON | cut -d ' ' -f 10 | xargs echo | sed -e 's/ /+/g' | bc -l)/$(grep -c MON $fname) | bc -l | xargs printf "%.2f\n" | sed -e "s/,/./g"
}

function getMax()
{
        cat $fname | grep MON | cut -d ' ' -f 10 | sort -n | tail -n 1
}

echo "#credit	parallel	size	rdma	ppn	Mean	Max" >> $DIR/stats.log

for ppn in 1
do
	if [ $ppn -eq 1 ]; then
			oneway=false
	else
			oneway=true
	fi

	for rdma in 256 #
	do
		for parallel in 1 32 #6
		do
			for size in  262144 2097152 #6
			do
				for credit in 2 4 #4
				do
					fname=$DIR/out-${credit}-${parallel}-${size}-${rdma}-${ppn}.log
					echo --------------------------------------------------------------------------
					echo
					echo "credit=${credit} parallel=${parallel} size=${size} rdma=${rdma} ppn=${ppn}"
					echo
					if [ -f $fname ]; then continue; fi
					genconf $buru
					#hydra-mpiexec -np 8 --ppn 1 --hostfile ~/hosts2 
					set -x
					mpirun -np 1  -npernode $ppn --hostfile ./hostfiles/em_intel $params $wrapper_intel  -f $DIR/config-scan.json -c $credit -t 12 : -np $buru  -npernode $ppn --hostfile ./hostfiles/a_10_1 $params $wrapper_amd  -f $DIR/config-scan.json -c $credit -t 12 : -np $buru -npernode $ppn --hostfile ./hostfiles/a_10_2 $params $wrapper_amd  -f $DIR/config-scan.json -c $credit -t 12 | tee $fname
					set +x
					cp $DIR/config-scan.json $fname.json
					echo "$credit	$parallel	$size	$rdma	$ppn	$(getMean)	$(getMax)" >> $DIR/stats.log
				done
			done
		done
	done
done

